# Java Reflection Homework

## Retrieving Class Members
1. Array of objects called sourceArray created in Main class. There are
two branches of inheritance: interface with four implementations (two
deprecated and two non-deprecated), and "normal" class Parent with two
subclasses (one deprecated and one not). Method findDeprecatedObjects()
working as it called and returns List of result.
2. I created class ProjectHierarchy which scanned project directory and
mapped superclasses or interfaces to their subclasses or implementations
except superclass Object. Also can be used lib "org.reflections" for 
this task. So non-deprecated substitutes get via object 
ProjectHierarchy. 