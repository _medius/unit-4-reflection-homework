package com.epam;

import com.epam.hierarchy.first.impl.*;
import com.epam.hierarchy.second.*;

import java.util.*;

public class Main {

	private List<Object> sourceArray;
	private ProjectHierarchy projectHierarchy = new ProjectHierarchy();

	@SuppressWarnings("deprecation")
	private void createObjectsList() {
		sourceArray = Arrays
				.asList(new DeprObjectOne(), new DeprObjectTwo(), new NonDeprObjectOne(), new NonDeprObjectTwo(),
						new Parent(), new DeprChild(), new NonDeprChild());
	}

	private List<Object> findDeprecatedObjects(List<?> objects) {
		List<Object> result = new ArrayList<>();

		for (Object object : objects) {
			Class currentClass = object.getClass();
			if (currentClass.isAnnotationPresent(Deprecated.class)) {
				result.add(object);

				System.out.println(currentClass + " is deprecated.");
				System.out.print("Use instead: ");

				List<Class> subTypes = projectHierarchy.getSubTypesOf(currentClass.getSuperclass());
				if (subTypes != null) {
					displaySubstitutes(subTypes);
				}

				Class[] interfaces = currentClass.getInterfaces();
				for (Class anInterface : interfaces) {
					subTypes = projectHierarchy.getSubTypesOf(anInterface);
					if (subTypes != null) {
						displaySubstitutes(subTypes);
					}
				}
				System.out.println();
			}
		}
		return result;
	}

	private void displaySubstitutes(List<Class> subTypes) {
		subTypes.stream()
				.filter(aClass -> !aClass.isAnnotationPresent(Deprecated.class))
				.forEach(System.out::println);
	}

	private void appStart() {
		createObjectsList();
		List<Object> resultArray = findDeprecatedObjects(sourceArray);
		System.out.println("\nResult array:");
		resultArray
				.forEach(System.out::println);
	}

	public static void main(String[] args) {
		new Main().appStart();
	}
}