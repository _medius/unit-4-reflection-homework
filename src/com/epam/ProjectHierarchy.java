package com.epam;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class ProjectHierarchy {

	private static final String EXTENSION = ".java";

	{
		findAllFilesInProject();
		filterFilesWithExtension();
		findSubTypes();
	}

	private List<File> allProjectFiles;
	private Map<Class, List<Class>> subTypes;

	private void findAllFilesInProject() {
		File rootDirectory = new File(System.getProperty("user.dir"));
		allProjectFiles = new ArrayList<>();

		scanDirectory(rootDirectory);
	}

	private void scanDirectory(File rootDirectory) {
		File[] nestedFiles = rootDirectory.listFiles();
		for (File file : nestedFiles) {
			if (file.isFile()) {
				this.allProjectFiles.add(file);
			} else {
				scanDirectory(file);
			}
		}
	}

	private void filterFilesWithExtension() {
		this.allProjectFiles = this.allProjectFiles
				.stream()
				.filter(file -> file.getName().endsWith(EXTENSION))
				.collect(Collectors.toList());
	}

	private void findSubTypes() {
		subTypes = new HashMap<>();
		try {
			for (File file : allProjectFiles) {
				Class currentClass = Class.forName(getNameForLoader(file));
				Class superClass = currentClass.getSuperclass();

				mapSubType(currentClass, superClass);
			}

			for (File file : allProjectFiles) {
				Class currentClass = Class.forName(getNameForLoader(file));
				Class[] interfaces = currentClass.getInterfaces();

				for (Class anInterface : interfaces) {
					mapSubType(currentClass, anInterface);
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void mapSubType(Class currentClass, Class superClass) {
		if (superClass != null && !superClass.equals(Object.class)) {
			if (subTypes.containsKey(superClass)) {
				subTypes.get(superClass).add(currentClass);
			} else {
				List<Class> subClasses = new ArrayList<>();
				subClasses.add(currentClass);
				subTypes.put(superClass, subClasses);
			}
		}
	}

	private String getNameForLoader(File file) {
		int beginIndex = file.getPath().indexOf('\\', System.getProperty("user.dir").length() + 1) + 1;
		int endIndex = file.getPath().indexOf(EXTENSION);
		String pathInProject = file.getPath().substring(beginIndex, endIndex);
		return pathInProject.replace('\\', '.');
	}

	public List<Class> getSubTypesOf(Class parent) {
		return subTypes.get(parent);
	}
}
